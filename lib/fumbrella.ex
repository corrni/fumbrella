defmodule Fumbrella do
  @moduledoc """
  Documentation for Fumbrella.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Fumbrella.hello
      :world

  """
  def hello do
    :world
  end
end
